/****************************************************************************************************/
/**
  \file         Utility.c
  \brief        Utilities as CRC, global const, errors definitions, address of external memories.
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Mar 10, 2020

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Utility.h"

/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Variables EXTERN                                             *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Calculates the CRC 4 of a block of data using LUTs
* \author   Gerardo Valdovinos
* \return   CRC value (u8)
*****************************************************************************************************/
u8 u8fnUtilityCrc4(u8 u8Size, u8 *pu8Data)
{
    u8 u8CheckSum, i;
    u8 au8CrcLookup[16] = {0, 13, 7, 10, 14, 3, 9, 4, 1, 12, 6, 11, 15, 2, 8, 5};
    
    /* Initialize seed with 0101. */
    u8CheckSum = 5;
    for(i = 0;i < u8Size;i++) 
    {
        u8CheckSum = u8CheckSum ^ pu8Data[i];
        u8CheckSum = au8CrcLookup[u8CheckSum];
    };
    return (u8CheckSum);
}

/***************************************End of Functions Definition**********************************/




