/****************************************************************************************************/
/**
  \file         Typedefs.h
  \brief        
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Sep 5, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

#ifndef __TYPEDEFS_H   /*Duplicated Includes Prevention*/
#define __TYPEDEFS_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include <avr/io.h>
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define FOSC		16000000u
#define NULL		((void *)0)

#define OK			1u
#define NOK			0u
#define ON			1u
#define OFF			0u
#define BUSY		2u
#define INVALID		0xFFu
#define TRUE		1u
#define FALSE		0u
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef unsigned char			u8;
typedef char					s8;
typedef unsigned short			u16;
typedef short					s16;
typedef unsigned int			u32;
typedef int						s32;
typedef void  ( * tpvfn )( void  );
typedef void  ( * const tcpvfn)( void  );
typedef void  ( * tpu8fn )( u8  );
typedef void  ( * const tcpu8fn)( u8  );

typedef unsigned char			uint8;
typedef unsigned short			uint16;
typedef unsigned int			uint32;
typedef int						sint32;
/* 8-Bit */
typedef unsigned       char BYTE;
typedef   signed       char SHORTINT;

/* 16-Bit */
typedef unsigned short      WORD;
typedef   signed short      INTEGER;

/* 32-Bit */
typedef unsigned       long LONGWORD;
typedef   signed       long LONGINT;

/*****************************************************************************************************
*                                          Variables EXTERN                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

#endif /* __TYPEDEFS_H */
/***************************************End of File**************************************************/
