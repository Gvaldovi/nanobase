/****************************************************************************************************/
/**
  \file         TaskScheduler.c
  \brief        Scheduler for tasks.
  \author       Gerardo Valdovinos
  \project      
  \version      0.0.1
  \date         Mar 24, 2015

  This code ...
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "TaskScheduler.h"
#include <string.h>
#include <avr/io.h>

/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                          Extern functions                                          *
*****************************************************************************************************/
/* Tasks functions */
extern void vfnRelaysE(u8 Param);
extern void vfnSentE(u8 Param);

/* Timed tasks functions */
extern void vfnLedP(u8 Param);
// extern void vfnTaskO(u8 Param);
/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/*****************************************************************************************************
*                                         Definition of CONSTs                                       *
*****************************************************************************************************/
/**
 *  TASKS
 **/
const tpu8fn afnTasksList[TASK_LIST_SIZE] =
{
       [tRelaysE]   = vfnRelaysE,
	   [tSentE]		= vfnSentE,
};

/**
 *  TIMED TASKS
 **/
const tpu8fn afnTimedTasksList[TIMED_TASK_LIST_SIZE] =
{
       [tLedP] = vfnLedP,
    //    [tTaskO] = vfnTaskO
};

/* Masks for byte */
const u8 au8BitMask[] = {1,2,4,8,16,32,64,128};

/*****************************************************************************************************
*                                       Definition of VARIABLEs                                      *
*****************************************************************************************************/
u8 au8TaskReady[((TASK_LIST_SIZE - 1) / 8) + 1];
u8 au8TaskParam[TASK_LIST_SIZE];

#if USE_TIMED_TASK
u8  au8TimedTaskReady[((TIMED_TASK_LIST_SIZE - 1) / 8) + 1];
u8  au8TimedTaskParam[TIMED_TASK_LIST_SIZE];
u16 au16TimedTaskValues[TIMED_TASK_LIST_SIZE];
u16 au16TimedTaskCounters[TIMED_TASK_LIST_SIZE];
#endif

u8 u8SystemTick;
/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        METADATA INFORMATION                                        *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Definition of FUNCTIONS                                      *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Task scheduler initialization.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
void vfnTaskSchedulerInit(void)
{
	/* Clear Tasks control arrays */
    memset(&au8TaskReady, 0, sizeof(au8TaskReady));
    memset(&au8TaskParam, 0, sizeof(au8TaskParam));

#if USE_TIMED_TASK
    memset(&au8TimedTaskReady,		0, sizeof(au8TimedTaskReady));
    memset(&au8TimedTaskParam,		0, sizeof(au8TimedTaskParam));
    memset(&au16TimedTaskCounters,	0, sizeof(au16TimedTaskCounters));
    memset(&au16TimedTaskValues,	0, sizeof(au16TimedTaskValues));
#endif
}

/*****************************************************************************************************
* \brief    Task scheduler.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
void vfnTaskScheduler(void)
{
	/* Index */
	register u8 u8Index = TASK_LIST_SIZE - 1;

	/* Pointer to tasks */
	register tpu8fn *pu8fnTask = (tpu8fn*) &afnTasksList[ TASK_LIST_SIZE - 1 ];

	/* Pointer to ready tasks */
	register u8 *pu8Ready = &au8TaskReady[ TASK_LIST_SIZE / 8 ];

	/* Loop for executing tasks */
	do
	{
		if( (*pu8Ready) & au8BitMask[u8Index % 8])
		{
			if(au8TaskParam[u8Index] & SELF_CLEARING_EVENT_FLAG)
			{
				/* Auto clearing task */
				(*pu8Ready) &= ~au8BitMask[u8Index % 8];

				/* Quit self clearing event flag of parameter */
				au8TaskParam[u8Index] &= 0x7F;
			}

			/* Run task with its parameter */
			(*pu8fnTask)(au8TaskParam[u8Index]);

		}
		if(!(u8Index % 8))
			pu8Ready--;

		pu8fnTask--;
	}while(u8Index--);

#if USE_TIMED_TASK
	if(u8SystemTick)
	{
		u8SystemTick = 0;
		/* Execute timed tasks */
		vfnTaskSchedulerTimedTask();
	}
#endif
}

/*****************************************************************************************************
* \brief    Task scheduler enable task.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    u8Param MUST be less than 0x7F or 127
******************************************************************************************************/
void vfnTaskSchedulerEnableT(teTasks eTaskID, u8 u8Param, u8 u8SelfClearing)
{
	if(TASK_LIST_SIZE > eTaskID)
	{
		if(u8SelfClearing)
			u8Param |= SELF_CLEARING_EVENT_FLAG;

		au8TaskParam[eTaskID] = u8Param;
		eTaskID %= 8;
		au8TaskReady[eTaskID / 8] |= au8BitMask[eTaskID];
	}
}

/*****************************************************************************************************
* \brief    Task scheduler disable task.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
void vfnTaskSchedulerDisableT(teTasks eTaskID)
{
	if(TASK_LIST_SIZE > eTaskID)
	{
		au8TaskParam[eTaskID] = 0;
		eTaskID %= 8;
		au8TaskReady[eTaskID / 8] &= ~au8BitMask[eTaskID];
	}
}


#if USE_TIMED_TASK
/*****************************************************************************************************
* \brief    Task scheduler timed tasks. Periodic and Oneshot.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
void vfnTaskSchedulerTimedTask(void)
{
	/* Index */
	register u8 u8Index = TIMED_TASK_LIST_SIZE - 1;

	/* Pointer to tasks */
	register tpu8fn *pu8fnTask = (tpu8fn*) &afnTimedTasksList[TIMED_TASK_LIST_SIZE - 1];

	/* Pointer to tasks flags */
	register u16 *pu16Counter = &au16TimedTaskCounters[TIMED_TASK_LIST_SIZE - 1];

	/* Pointer to ready tasks */
	register u8 *pu8Ready = &au8TimedTaskReady[(TIMED_TASK_LIST_SIZE - 1) / 8];

	do
	{
		if( (*pu8Ready) & au8BitMask[u8Index % 8] )
		{
			if(!(--(*pu16Counter)))
    		{
				if(au8TimedTaskParam[u8Index] & SELF_CLEARING_EVENT_FLAG)
				{
					/* Auto clearing task */
					(*pu8Ready) &= ~au8BitMask[u8Index % 8];

					/* Quit self clearing event flag of parameter */
					au8TimedTaskParam[u8Index] &= 0x7F;
				}

				*pu16Counter = au16TimedTaskValues[u8Index];
				(*pu8fnTask)(au8TimedTaskParam[u8Index]);
			}
		}

		if(!(u8Index % 8))
			pu8Ready--;

		pu16Counter--;
		pu8fnTask--;

	}while(u8Index--);
}

/*****************************************************************************************************
* \brief    Task scheduler enable timed task.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    u8Param MUST be less than 0x7F or 127
******************************************************************************************************/
void vfnTaskSchedulerEnableTT(teTTasks eTTasksID, u8 u8Param, u8 u8SelfClearing, u16 u16Period)
{
	if(TIMED_TASK_LIST_SIZE > eTTasksID)
	{
		if(u8SelfClearing)
			u8Param |= SELF_CLEARING_EVENT_FLAG;

		au8TimedTaskReady[(eTTasksID / 8)] |= au8BitMask[eTTasksID % 8];
		au16TimedTaskCounters[eTTasksID] = u16Period;
		au16TimedTaskValues[eTTasksID]	= u16Period;
		au8TimedTaskParam[eTTasksID] = u8Param;
	}
}

/*****************************************************************************************************
* \brief    Task scheduler disable timed task.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes    
******************************************************************************************************/
void vfnTaskSchedulerDisableTT(teTTasks eTTasksID)
{
	if(TIMED_TASK_LIST_SIZE > eTTasksID)
	{
		au8TimedTaskReady[(eTTasksID / 8)] &= ~au8BitMask[eTTasksID % 8];
	}
}

#endif

/*********************************************End of File********************************************/
