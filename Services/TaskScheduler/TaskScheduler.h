/****************************************************************************************************/
/**
  \file         TaskScheduler.h
  \brief
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Sep 5, 2015

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

#ifndef __TASKSCHEDULER_H_
#define __TASKSCHEDULER_H_

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "Typedefs.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define USE_TIMED_TASK                      1u
#define SELF_CLEARING_EVENT_FLAG            0x80u
/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
/* Tasks enum */
typedef enum
{
    tRelaysE = 0,
    tSentE,
    TASK_LIST_SIZE
}teTasks;

/* Timed tasks enum */
typedef enum
{
    tLedP = 0,
    TIMED_TASK_LIST_SIZE
}teTTasks;
/*****************************************************************************************************
*                                          Variables EXTERN                                        *
*****************************************************************************************************/
/* System tick variable */
extern u8 u8SystemTick;
/*****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnTaskSchedulerInit(void);

void vfnTaskScheduler(void);
void vfnTaskSchedulerEnableT(teTasks eTaskID, u8 u8Param, u8 u8SelfClearing);
void vfnTaskSchedulerDisableT(teTasks eTaskID);

void vfnTaskSchedulerTimedTask(void);
void vfnTaskSchedulerEnableTT(teTTasks eTTasksID, u8 u8Param, u8 u8SelfClearing,  u16 u16Period);
void vfnTaskSchedulerDisableTT(teTTasks eTTasksID);


#endif /* __TASKSCHEDULER_H_ */
/***************************************End of File**************************************************/
