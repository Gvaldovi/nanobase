/****************************************************************************************************/
/**
  \file         Adc.c
  \brief		Adc driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Apr 6, 2018

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include "Adc.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/

/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Configuration of a I/O pin. Speed is 50Mhz as default.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnAdcInit(const tsAdcChannelCfg *ChannelCfg)
{
	ADCSRA;
	ADCSRB;
	ADMUX;
}

/*****************************************************************************************************
* \brief    Set a I/O pin
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnAdcPower(u8 u8AdcPower)
{
	if(u8AdcPower == ON)
		ADCSRA |= (1 << ADEN);
	else
		ADCSRA &= ~(1 << ADEN);
}
/***************************************End of Functions Definition**********************************/
