/****************************************************************************************************/
/**
  \file         Gpio.h
  \brief		Gpio driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Aug 12, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __GPIO_H
#define	__GPIO_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "GpioCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/
#define GPIO_ADDRESS_BASE					0x23u
#define GPIO_ADDRESS_OFFSET					3u

#define GPIO_PIN(p)							*(u8*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 0)
#define GPIO_PIN_SET(Port, Pin)				GPIO_PIN(Port) |= (1 << Pin)
#define GPIO_PIN_GET(Port, Pin)				(GPIO_PIN(Port) & (1 << Pin)) == (1 << Pin)
#define GPIO_PIN_READ(Port)					(u8)GPIO_PIN(Port)

#define GPIO_DDR(p)							*(u8*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 1)
#define GPIO_DDR_SET(Port, Pin)				GPIO_DDR(Port) |= (1 << Pin)

#define GPIO_PORT(p)						*(u8*)(GPIO_ADDRESS_BASE + (GPIO_ADDRESS_OFFSET * p) + 2)
#define GPIO_PORT_SET(Port, Pin)			GPIO_PORT(Port) |= (1 << Pin)
#define GPIO_PORT_RESET(Port, Pin)			GPIO_PORT(Port) &= ~(1 << Pin)
#define GPIO_PORT_WRITE(Port, Value)		GPIO_PORT(Port) = (u8)Value

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnGpioInit(const tsGpioDriverCfg *DriverCfg);

void vfnGpioCfg(tsGpioChannelCfg *GpioCfg);
void vfnGpioSet(teGpioPort ePort, u8 u8Pin);
u8 u8fnGpioGet(teGpioPort ePort, u8 u8Pin);
void vfnGpioReset(teGpioPort ePort, u8 u8Pin);
void vfnGpioToggle(teGpioPort ePort, u8 u8Pin);
void vfnGpioWrite(teGpioPort ePort, u8 u8Value);
u8 u8fnGpioRead(teGpioPort ePort);

#endif	/* __GPIO_H */
/***************************************End of File**************************************************/
