/**
  \file         I2c.h
  \brief		    I2c driver headers.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Aug 12, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/
#ifndef __I2C_H
#define	__I2C_H

/*****************************************************************************************************
*                                            Include files                                           *
*****************************************************************************************************/
#include "I2cCfg.h"
/*****************************************************************************************************
*                                              #DEFINEs                                              *
*****************************************************************************************************/
#define I2C_OFF          		0u
#define I2C_ON					1u
#define I2C_BUSY				2u

#define I2C_START               0x08u
#define I2C_SLA_ACK             0x18u
#define I2C_DATA                0x50u

/*****************************************************************************************************
*                                               #MACROs                                              *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Declaration of module TYPEs                                    *
*****************************************************************************************************/
typedef struct
{
    u8         u8Status;				/* Status of driver */
    u16        u16Size;         /* Size */
    u8*        pu8Data;         /* Pointer to data */
    u8         u8Stop;          /* Stop condition */        
    tpvfn      pvfnCallback;	  /* Transmission callback */
}tsI2cChannel;

/****************************************************************************************************
*                                   Declaration of module FUNCTIONs                                  *
*****************************************************************************************************/
void vfnI2cInit(const tsI2cChannelCfg *psI2cChannelCfg);

void vfnI2cEnable(void);
void vfnI2cDisable(void);
void vfnI2cSetCallback(tpvfn pvfnCallback);
u8 u8fnI2cSend(u16 u16Size, u8 *pu8Data, u8 u8Stop);

#endif	/* __I2C_H */

/***************************************End of File**************************************************/

