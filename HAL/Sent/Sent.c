/****************************************************************************************************/
/**
  \file         Sent.c
  \brief        Protocol SENT using the 16bit Timer1.
  \author       Gerardo Valdovinos
  \project      TaskScheduler
  \version      0.0.1
  \date         Feb 18, 2020

  Program compiled with "gcc-avr",
  Tested and programmed with ATAvrDragon in isp mode.

*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include <avr/interrupt.h>
#include <avr/io.h>
#include <string.h>

#include "Sent.h"
#include "Gpio.h"
#include "Uart.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
// volatile tsSent asSent[SENT_CHANNEL_MAX];
u8 au8SentData[20];
tsSentData sSentData;
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/
void vfnSentCrc(u8 *pu8Data, u8 u8Size);
/*****************************************************************************************************
*                                    METADATA INFORMATION                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                       Definition of FUNCTIONS                                      *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Sent initialization. Timer1 enable for working at 1000 Hz.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
void vfnSentInit(void)
{
    // Test with higher period to see the led.
    // TCCR1A = 0;
    // TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);       // Prescaler = 8
    // OCR1AH = 0x0F;                                 // For a 3us tick. 16MHz / 8 = 2MHz -> (1 / 2MHz) * 6 = 3us
    // OCR1AL = 0x42;
    // TIMSK1 = (1 << OCIE1A);                     // Interrupt enable

    /* Set up Sent timer to 1ms */
    TCCR1A = 0;
    TCCR1B |= (1 << WGM12) | (1 << CS11);               // Prescaler = 8
    OCR1AH = (u8)((SENT_TICK_90_US & 0xFF00) >> 8u);    // For a 90us tick. 16MHz / 8 = 2MHz -> (1 / 2MHz) * 180 = 90us
    OCR1AL = (u8)((SENT_TICK_90_US & 0x00FF) >> 0u);
    // TIMSK1 = (1 << OCIE1A);                             // Interrupt enable

    /* GPIO output */
    PORTD |= (1 << PD5);

    /* Save the status ON. */
    sSentData.u8Status = ON;
}

/*****************************************************************************************************
* \brief    Configure Tick time.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
void vfnSentSetTick(u16 u16Tick)
{
    OCR1AH = (u8)((u16Tick & 0xFF00) >> 8u);
    OCR1AL = (u8)((u16Tick & 0x00FF) >> 0u);
}

/*****************************************************************************************************
* \brief    Configure Tick time.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
u8 u8fnSentStatus(void)
{
    return sSentData.u8Status;
}

/*****************************************************************************************************
* \brief    Send SENT message.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
******************************************************************************************************/
u8 u8fnSentSendMsg(u8 u8Comm, u8 u8MsgSize, u8 *pu8Msg)
{
    u8 i;
    u8 u8Status = NOK;

    if(sSentData.u8Status != BUSY && pu8Msg != NULL)
    {
        /* Copy sync and status */
        au8SentData[0] = 56u;
        au8SentData[1] = (u8Comm & 0xF) + 12u;

        /* Copy every message's byte plus 12 */
        for(i = 0;i < u8MsgSize;i++)
        {
            au8SentData[2+i] = *pu8Msg + 12u; 
            pu8Msg++;
        }

        /* Calculate CRC */ // For now added HARCODED
        vfnSentCrc(&au8SentData[0], (u8MsgSize + 2));

        /* Prepare the SENT data */
        sSentData.pu8TxData = &au8SentData[0];
        sSentData.u8Size = u8MsgSize + 3u;
        sSentData.u8TimeLow = 5u;
        sSentData.u8Status = BUSY;

        /* Transmission OK. */
        u8Status = OK;

        /* Start SENT message */
        TIMSK1 = (1 << OCIE1A);
    }

    return u8Status;
}

/*****************************************************************************************************
* \brief    Crc 4bit calculation.
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
void vfnSentCrc(u8 *pu8Data, u8 u8Size)
{
    // HARCODED
    pu8Data[u8Size] = 0x0F + 12;
}

/*****************************************************************************************************
* \brief    Interrupt Service Routine for Timer1 (Sent)
* \author   Gerardo Valdovinos
* \param    void
* \return   void
* \notes
******************************************************************************************************/
ISR(TIMER1_COMPA_vect)
{
    // Legacy code. Funciona.
    if((*sSentData.pu8TxData)--)
    {
        if(sSentData.u8TimeLow)
        {
            sSentData.u8TimeLow--;
            PORTD &= ~(1 << PD5);
        }
        else
            PORTD |= (1 << PD5);
    }
    else
    {
        if(sSentData.u8Size--)
        {
            sSentData.pu8TxData++;
            sSentData.u8TimeLow = 4;
            (*sSentData.pu8TxData)--;
            PORTD &= ~(1 << PD5);
        }
        else
        {
            TIMSK1 &= ~(1 << OCIE1A); 
            PORTD |= (1 << PD5);
            sSentData.u8Status = ON;
        }
    }


    // /* Verify if there is data to send */
    // if(sSentData.u8Size--)
    // {
    //     /* Decrease data. It will never be 0 */
    //     (*sSentData.pu8TxData)--;
        
    //     /* Put in High or Low the GPIO */
    //     if(sSentData.u8TimeLow)
    //     {
    //         sSentData.u8TimeLow--;
    //         PORTD &= ~(1 << PD5);
    //     }
    //     else
    //         PORTD |= (1 << PD5);

    //     /* If Data is 0 then change to the next */
    //     if(0u == *sSentData.pu8TxData)
    //     {
    //         sSentData.pu8TxData++;
    //         sSentData.u8TimeLow = 5;
    //     }
    // }
    // else
    // {
    //     /* Finish SENT message */
    //     TIMSK1 &= ~(1 << OCIE1A); 
    //     PORTD |= (1 << PD5);
    //     sSentData.u8Status = OK;
    // }

}
/***************************************End of Functions Definition**********************************/