/****************************************************************************************************/
/**
  \file         Uart.c
  \brief		Uart driver.
  \author       Gerardo Valdovinos
  \project      Nano
  \version      1.0.0
  \date         Sep 12, 2017

  Project compiled with WinAvr. THIS PROJECT HAS A BEER LICENSE.
*/
/****************************************************************************************************/

/*****************************************************************************************************
*                                        Include files                                               *
*****************************************************************************************************/
#include <string.h>
#include <avr/interrupt.h>
#include "Uart.h"
#include "Systick.h"
/*****************************************************************************************************
*                                           #MACROs                                                  *
*****************************************************************************************************/

/*****************************************************************************************************
*                                           #DEFINEs                                                 *
*****************************************************************************************************/

/*****************************************************************************************************
*                                  Declaration of module TYPEs                                       *
*****************************************************************************************************/

/*****************************************************************************************************
*                                     Definition of CONSTs                                           *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of VARIABLEs                                         *
*****************************************************************************************************/
volatile tsUartChannel sUartChannel;			/* Uart channel status */
tsUartChannelCfg *psUartChannelCfg;
teSystickChn eUartSystickChn;
/*****************************************************************************************************
*                                Declaration of module FUNCTIONs                                     *
*****************************************************************************************************/

/*****************************************************************************************************
*                                        Variables EXTERN                                            *
*****************************************************************************************************/

/*****************************************************************************************************
*                                    Definition of FUNCTIONS                                         *
*****************************************************************************************************/

/*****************************************************************************************************
* \brief    Initialization of Uart driver.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartInit(const tsUartChannelCfg *psChannelCfg)
{
	psUartChannelCfg = (tsUartChannelCfg *)psChannelCfg;

	/* Set status structure */
	sUartChannel.u8Status = ON;
	sUartChannel.u16TxSize = 0;
	sUartChannel.pu8TxData = NULL;
	sUartChannel.u16RxSize = 0;
	sUartChannel.pu8RxData = psUartChannelCfg->pu8RxBuffer;

	/* Configure Uart */
	UCSR0A |= (1 << U2X0);						// Select double speed.
	UCSR0B |= (1 << RXCIE0);					// Turn on Rx interrupt.
	UCSR0C |= (1 << UCSZ01) | (1 << UCSZ00);	// Select 8bit data.

	/* Set parity */
	UCSR0C |= (psUartChannelCfg->eParity << 4);

	/* Set baud rate */
	UBRR0H = (u8)(psUartChannelCfg->u16Baud >> 8);
	UBRR0L = (u8)(psUartChannelCfg->u16Baud);

	/* Set oneshot for Rx */
	eUartSystickChn = u8fnSystickSet(vfnUartRxTimeout);
	if(eUartSystickChn == INVALID)
		while(1); // Error
}

/*****************************************************************************************************
* \brief    Enable Uart channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartEnable(void)
{
	UCSR0B |= (1 << RXEN0) | (1 << TXEN0);
}

/*****************************************************************************************************
* \brief    Disable Uart channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartDisable(void)
{
	UCSR0B &= ~((1 << RXEN0) | (1 << TXEN0));
}

/*****************************************************************************************************
* \brief    Set Tx Callback.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartSetTxCallback(tpvfn	pvfnCallback)
{
	sUartChannel.pvfnTxCallback = pvfnCallback;
}

/*****************************************************************************************************
* \brief    Set Rx Callback.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartSetRxCallback(tpvfn	pvfnCallback)
{
	sUartChannel.pvfnRxCallback = pvfnCallback;
}

/*****************************************************************************************************
* \brief    Read Uart channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u16 u16fnUartRxSize(void)
{
	return sUartChannel.u16RxSize;
}

/*****************************************************************************************************
* \brief    Read Uart channel.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u16 u16fnUartRx(u8 *pu8Data)
{
	u16 u16TempSize;

	/* Copy to upper buffer */
	u16TempSize = sUartChannel.u16RxSize;
	memcpy(pu8Data, psUartChannelCfg->pu8RxBuffer, sUartChannel.u16RxSize);

	sUartChannel.pu8RxData = psUartChannelCfg->pu8RxBuffer;
	sUartChannel.u16RxSize = 0;

	return u16TempSize;
}

/*****************************************************************************************************
* \brief    Uart read function.
* \author	Gerardo Valdovinos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnUartTx(u16 u16Size, u8 *pu8Data)
{
	u8 u8Status = NOK;

	if(sUartChannel.u8Status != BUSY && pu8Data != NULL)
	{
		sUartChannel.pu8TxData = pu8Data;
		sUartChannel.u16TxSize = u16Size;
		sUartChannel.u8Status = BUSY;

		/* Turn on Tx interrupt */
		UCSR0B |= (1 << TXCIE0);

		/* Send first byte */
		UDR0 = *sUartChannel.pu8TxData++;

		u8Status = OK;
	}

	return u8Status;
}

/*****************************************************************************************************
* \brief    Uart status.
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
u8 u8fnUartStatus(void)
{
	return sUartChannel.u8Status;
}

/*****************************************************************************************************
* \brief    Uart Rx timeout for frame.
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
void vfnUartRxTimeout(void)
{
	/* A new frame has arrived */
	if(sUartChannel.pvfnRxCallback != NULL)
		sUartChannel.pvfnRxCallback();
}

/*****************************************************************************************************
* \brief    Uart Rx Interrupt Routine Service.
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
ISR(USART_RX_vect)
{
	/* Copy byte received */
	if(sUartChannel.u16RxSize < UART_RX_BUFFER_SIZE)
	{
		*sUartChannel.pu8RxData++ = UDR0;
		sUartChannel.u16RxSize++;
		vfnSystickStart(eUartSystickChn, 20);
	}
}

/*****************************************************************************************************
* \brief    Uart Tx Interrupt Routine Service.
* \author   Gerardo Valdovinos Villalobos
* \param    void
* \return   void
*****************************************************************************************************/
ISR(USART_TX_vect)
{
	if(--sUartChannel.u16TxSize)
	{
		UDR0 = *sUartChannel.pu8TxData++;
	}
	else
	{
		sUartChannel.u8Status = ON;
		sUartChannel.pu8TxData = NULL;
		sUartChannel.u16TxSize = 0;

		if(sUartChannel.pvfnTxCallback != NULL)
			sUartChannel.pvfnTxCallback();
	}
}

