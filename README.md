# README #

This repository is the base for Arduino Nano (atmega328p avr).
You need other complete project that contains a main.c.

### What is this repository for? ###

* Is the base of all projects for Nano device. Contains the HAL and TaskScheduler (event manager) functions).
* Version 1.0.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact